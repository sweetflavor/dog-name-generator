import Vue from 'vue'
import Vuex, { createNamespacedHelpers } from 'vuex'
import axios from 'axios'

Vue.use(Vuex, axios)

export default new Vuex.Store({
    state: {
        dogs: [],
        selected: '',
        random_dog: '',
        picked_gender: 'female',
        generatName: ''
    },
    actions: {
       loadDogs ({commit}) {
         axios
            .get('https://dog.ceo/api/breeds/list/all')
            .then(dogs => {
                let arr = []
  
                Object.keys(dogs.data.message).forEach((dog)=> {
                    if (dogs.data.message[dog].length === 0) {
                        arr.push(dog);
                    } else {
                        dogs.data.message[dog].forEach(type => {
                            arr.push(`${dog} ${type}`);
                        });
                    } 
                })

                commit('SET_DOGS', arr)

            })
        },

        loadRandomDog({commit}) {
            axios
                .get('https://dog.ceo/api/breeds/image/random')
                .then(random_dog => {
                    let randomDog = random_dog.data.message

                    commit('SET_RANDOM_DOG', randomDog)

                }).catch(error => {
                    console.log(error)
                })
        },

        dogGen({commit}, val) {
            commit('SET_DOG_GEN', val)
        },

        generateName({commit}) {
            console.log(this.state)
            axios
                .get(`https://randomuser.me/api/?gender=${this.state.picked_gender}`)
                .then(data => {
                    let name = data.data.results[0].name.first
                    console.log(name, 55)
                    commit('SET_DOG_NAME', name)
                })
                .catch(error => {
                    console.log(error)
                })
        }
    },

    mutations: {
        SET_DOGS (state, dogs) {
            state.dogs = dogs
        },
        SET_RANDOM_DOG (state, randomDog) {
            state.random_dog = randomDog
        },
        SET_DOG_GEN (state, dogGen) {
            state.picked_gender = dogGen
        },
        SET_DOG_NAME (state, name) {
            state.generatName = name
        }
    }
})